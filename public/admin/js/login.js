document.addEventListener("DOMContentLoaded", function () {
	const firestore = firebase.firestore(); // eslint-disable-line no-undef, no-unused-vars
	const settings = {
		timestampsInSnapshots: true
	};
	firestore.settings(settings);
	const btn = document.querySelector("button");
	
	btn.addEventListener("click", function(){
		firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION) // eslint-disable-line no-undef, no-unused-vars
			.then(function() {
				const username = document.querySelector("#username").value;
				const password = document.querySelector("#password").value;
				firebase.auth().signInWithEmailAndPassword(username, password); // eslint-disable-line no-undef, no-unused-vars
			})
			.catch(function(error) {
				console.log(error);
			});
	});

	
	firebase.auth().onAuthStateChanged(function(user) { // eslint-disable-line no-undef, no-unused-vars
		if (user !== null) {
			window.location = "./adminpanel.html";
		}else {
			console.log("not logged in");
		}
	});
});