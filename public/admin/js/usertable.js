document.addEventListener("DOMContentLoaded", function () {
	const actionButton = document.querySelectorAll(".fixed-action-btn");
	const actionButtonInstances = M.FloatingActionButton.init(actionButton);// eslint-disable-line no-undef, no-unused-vars

	const tbody = document.querySelector("tbody");

	// * init firestore
	const firestore = firebase.firestore(); // eslint-disable-line no-undef, no-unused-vars
	const settings = {
		timestampsInSnapshots: true
	};
	firestore.settings(settings);

	// * get the table runnin
	let table = "";
	firestore.collection("users").onSnapshot(querySnapshot => {
		querySnapshot.forEach(doc => {
			
			if(doc.data().stonebottom === undefined){
				console.log(doc);
				let data = doc.data();
				table += `<tr class="modal-trigger" data-target="modalUpdate" data-href="${doc.id}">
							<td>${data.voornaam} ${data.tussenvoegsel} ${data.achternaam}</td>
							<td>${data.geslacht}</td>
							<td>${data.geboortedatum}</td>
						</tr>`;
			}
		});
		tbody.innerHTML = table;
		table = "";
		// * init the modal with data
		tbody.addEventListener("click", function (e) {
			let specificCategorie = firestore.collection("users").doc(e.originalTarget.parentNode.dataset.href).get();

			specificCategorie
				.then(doc => {
					const modalContent = document.querySelector(".modal-content");
					let data = doc.data();

					let content = `
									<h3></h3>
									<p>${data.geboortedatum}</p>
									<p></p>
									<p></p>
									<p></p>
									<p></p>
									
									<ul class="collection with-header">
										<li class="collection-header"><h4>${data.voornaam} ${data.tussenvoegsel} ${data.achternaam}</h4></li>
										<li class="collection-item"><span class="chip">geslacht</span> ${data.geslacht}</li>
										<li class="collection-item"><span class="chip">postcode</span>${data.postcode}</li>
										<li class="collection-item"><span class="chip">woonplaats</span>${data.woonplaats}</li>
										<li class="collection-item"><span class="chip">huisnummer</span>${data.huisnummer}</li>
									</ul>
								`;
					modalContent.innerHTML = content;
				});
		});
		const modal = document.querySelectorAll(".modal");
		const modalInstances = M.Modal.init(modal); // eslint-disable-line no-undef, no-unused-vars
	});
});