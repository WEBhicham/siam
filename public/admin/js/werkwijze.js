document.addEventListener("DOMContentLoaded", function () {

	// Global variables
	const actionButton = document.querySelectorAll(".fixed-action-btn");
	const actionButtonInstances = M.FloatingActionButton.init(actionButton); // eslint-disable-line no-undef, no-unused-vars
	const tbody = document.querySelector("tbody");
	const btnSaveUpdate = document.querySelector("#modalUpdate > .modal-footer > .btnSave");
	const btnDelete = document.querySelector("#modalUpdate > .modal-footer > .btnDelete");
	const btnSaveAdd = document.querySelector("#modalAdd > .modal-footer > .btnSave");

	// init firestore
	const firestore = firebase.firestore(); // eslint-disable-line no-undef, no-unused-vars
	const settings = {
		timestampsInSnapshots: true
	};
	firestore.settings(settings);

	firestore.collection("werkwijze").onSnapshot(querySnapshot => {
		let table = "";
		querySnapshot.forEach(doc => {
			if(doc.data().stonebottom === undefined){
				tbody.innerHTML = "";
				let data = doc.data();
				table += `
					<tr class="modal-trigger" data-target="modalUpdate">
						<td>${data.icon}</td>
						<td>${data.titel}</td>
						<td>${data.tekst}</td>
						<td>${data.order}</td>
					</tr>
				`;
			}
		});
		tbody.insertAdjacentHTML("beforeend", table);

		tbody.addEventListener("click", function (e) {
			const icon = e.target.parentNode.children[0].textContent;
			const title = e.target.parentNode.children[1].textContent;
			const text = e.target.parentNode.children[2].textContent;
			const order = e.target.parentNode.children[3].textContent;
			
			let specificCategorie = firestore.collection("werkwijze")
				.where("icon", "==", icon)
				.where("titel", "==", title)
				.where("tekst", "==", text)
				.where("order", "==", order)
				.get();
			
			specificCategorie
				.then(querySnapshot => {
					querySnapshot.forEach(doc => {
					
						let data = doc.data();
						
						const icon = document.querySelector("#modalUpdate .icon");
						const title = document.querySelector("#modalUpdate .title");
						const text = document.querySelector("#modalUpdate .text");
						const order = document.querySelector("#modalUpdate .order");
						
						icon.value = data.icon;
						title.value = data.titel;
						text.value = data.tekst;
						order.value = data.order;
					});
				});
	
			btnSaveUpdate.addEventListener("click", function(){
	
				const icon = document.querySelector("#modalUpdate .icon");
				const title = document.querySelector("#modalUpdate .title");
				const text = document.querySelector("#modalUpdate .text");
				const order = document.querySelector("#modalUpdate .order");
		
				const changeObject = {
					icon: icon.value,
					titel: title.value,
					tekst: text.value,
					order: order.value
				};
		
				specificCategorie
					.then(querySnapshot => {
						querySnapshot.forEach(doc => {
							doc.ref.update(changeObject);
						});
					});
			});
	
			btnDelete.addEventListener("click", function () {
				specificCategorie
					.then(querySnapshot => {
						querySnapshot.forEach(doc => {	
							doc.ref.delete();
						});
					});
			});
		});

		btnSaveAdd.addEventListener("click", function(){
			const icon = document.querySelector("#modalAdd .icon");
			const title = document.querySelector("#modalAdd .title");
			const text = document.querySelector("#modalAdd .text");
			const order = document.querySelector("#modalAdd .order");
	
			const collection = firestore.collection("werkwijze");
			collection.add({
				icon: icon.value,
				titel: title.value,
				tekst: text.value,
				order: order.value
			});
		});
	});
});