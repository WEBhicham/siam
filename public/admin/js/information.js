document.addEventListener("DOMContentLoaded", function () {
	// Global variables
	const actionButton = document.querySelectorAll(".fixed-action-btn");
	const actionButtonInstances = M.FloatingActionButton.init(actionButton); // eslint-disable-line no-undef, no-unused-vars
	const tbody = document.querySelector("tbody");
	const btnSaveUpdate = document.querySelector("#modalUpdate > .modal-footer > .btnSave");
	const btnDelete = document.querySelector("#modalUpdate > .modal-footer > .btnDelete");
	const btnSaveAdd = document.querySelector("#modalAdd > .modal-footer > .btnSave");
	// init firestore
	const firestore = firebase.firestore(); // eslint-disable-line no-undef, no-unused-vars
	const settings = {
		timestampsInSnapshots: true
	};
	firestore.settings(settings);

	firestore.collection("information").onSnapshot(querySnapshot => {
		let table = "";
		querySnapshot.forEach(doc => {
			if(doc.data().stonebottom === undefined){
				tbody.innerHTML = "";
				let data = doc.data();
				table += `
					<tr class="modal-trigger" data-target="modalUpdate">
						<td>${data.titel}</td>
						<td>${data.preTekst}</td>
						<td>${data.informatie}</td>
					</tr>
				`;
			}
		});
		tbody.insertAdjacentHTML("beforeend", table);
	});


	tbody.addEventListener("click", function (e) {
		const title = e.target.parentNode.children[0].textContent;
		const preText = e.target.parentNode.children[1].textContent;
		const text = e.target.parentNode.children[2].textContent;

		let specificCategorie = firestore.collection("information")
			.where("titel", "==", title)
			.where("preTekst", "==", preText)
			.where("informatie", "==", text)
			.get();

		specificCategorie
			.then(querySnapshot => {
				querySnapshot.forEach(doc => {
					let data = doc.data();
					const title = document.querySelector("#modalUpdate .title");
					const preText = document.querySelector("#modalUpdate .preText");
					const text = document.querySelector("#modalUpdate .text");

					title.value = data.titel;
					preText.value = data.preTekst;
					text.value = data.informatie;
				});
			});

		btnSaveUpdate.addEventListener("click", function(){

			const contentTitle = document.querySelector("#modalUpdate .title");
			const contentPreText = document.querySelector("#modalUpdate .preText");
			const contentText = document.querySelector("#modalUpdate .text");
	
			const changeObject = {
				titel: contentTitle.value,
				preTekst: contentPreText.value,
				informatie: contentText.value,
			};
	
			specificCategorie
				.then(querySnapshot => {
					querySnapshot.forEach(doc => {
						doc.ref.update(changeObject);
					});
				});
		});

		btnDelete.addEventListener("click", function () {
			specificCategorie
				.then(querySnapshot => {
					querySnapshot.forEach(doc => {	
						doc.ref.delete();
					});
				});
		});
	});

	btnSaveAdd.addEventListener("click", function(){
		const contentTitle = document.querySelector("#modalAdd .title");
		const contentPreText = document.querySelector("#modalAdd .preText");
		const contentText = document.querySelector("#modalAdd .text");

		const collection = firestore.collection("information");
		collection.add({
			titel: contentTitle.value,
			preTekst: contentPreText.value,
			informatie: contentText.value
		});
	});
});
