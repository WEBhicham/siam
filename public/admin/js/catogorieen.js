document.addEventListener("DOMContentLoaded", function () {
	// /*eslint-disable no-unused-vars*/
	// Global variables
	const actionButton = document.querySelectorAll(".fixed-action-btn");
	const actionButtonInstances = M.FloatingActionButton.init(actionButton); // eslint-disable-line no-undef, no-unused-vars

	const body = document.querySelector("body");
	const tbody = document.querySelector("tbody");
	const btnSaveUpdate = document.querySelector("#modalUpdate > .modal-footer > .btnSave");
	const btnDelete = document.querySelector("#modalUpdate > .modal-footer > .btnDelete");
	const btnSaveAdd = document.querySelector("#modalAdd > .modal-footer > .btnSave");

	// init firestore
	const firestore = firebase.firestore(); // eslint-disable-line no-undef, no-unused-vars
	const settings = {
		timestampsInSnapshots: true
	};
	firestore.settings(settings);

	// get the table runnin
	firestore.collection("categories").onSnapshot(querySnapshot => {
		querySnapshot.forEach(doc => {
			if(doc.data().stonebottom === undefined){
				tbody.innerHTML = "";
				let storageRef = firebase.storage().ref("images/");// eslint-disable-line no-undef, no-unused-vars
				let data = doc.data();
				let imgref = storageRef.child(data.img).getDownloadURL();
				
				imgref.then(url => {
					let table = `<tr class="modal-trigger" data-target="modalUpdate" data-href="${data.id}">
					<td>${data.name}</td> 	
					<td class="description">${data.description}</td>
					<td><img src="${url}"></td>
					<td>${data.subs}</td>
					</tr>`;
					tbody.insertAdjacentHTML("beforeend", table);
				});
			}
		});
		
	});
	
	// ** init the modal with data
	tbody.addEventListener("click", function (e) {
		let specificCategorie = firestore.collection("categories").where("id", "==", e.originalTarget.parentNode.dataset.href).get();
		specificCategorie
			.then(querySnapshot => {
				
				const modalId = document.querySelector("#modalUpdate .modal-content > input#disabled");
				const modalContentTitle = document.querySelector("#modalUpdate .modal-content > input:not(#disabled)");
				const modalContentDescription = document.querySelector("#modalUpdate .modal-content textarea");
				const modalContentImg = document.querySelector("#modalUpdate .modal-content .file-path");
				const modalContentShow = document.querySelector("#modalUpdate .modal-content .img");
				const tabs = document.querySelectorAll("#modalUpdate .tab");

				querySnapshot.forEach(doc => {
					let data = doc.data();

					const imgref = firebase.storage().ref(`images/${data.img}`); // eslint-disable-line no-undef, no-unused-vars
					const downloadurl = imgref.getDownloadURL();

					downloadurl.then(url => {
						modalId.value = data.id;
						modalContentTitle.value = data.name;
						modalContentDescription.value = data.description;
						modalContentImg.value = data.img;
						modalContentShow.innerHTML = `<img src="${url}">`;
						tabs[0].children[0].value = data.tabs.tab1.title;
						tabs[0].children[1].value = data.tabs.tab1.text;

						tabs[1].children[0].value = data.tabs.tab2.title;
						tabs[1].children[1].value = data.tabs.tab2.text;

						tabs[2].children[0].value = data.tabs.tab3.title;
						tabs[2].children[1].value = data.tabs.tab3.text;
						
					});
				});
			});
	});

	// * save updates 
	btnSaveUpdate.addEventListener("click", function () {
		const modalId = document.querySelector(".modal-content > input#disabled");
		const modalContentTitle = document.querySelector(".modal-content > input:not(#disabled)");
		const modalContentDescription = document.querySelector(".modal-content textarea");
		const modalContentImg = document.querySelector("#modalUpdate > .modal-content .fileInput").files[0];
		const tabs = document.querySelectorAll("#modalUpdate .tab");

		let storageRef = firebase.storage().ref("images/");// eslint-disable-line no-undef, no-unused-vars
		let imgRef = storageRef.child(getId()); 

		imgRef.put(modalContentImg).then(snapshot => {
			const changeObject = {
				name: modalContentTitle.value,
				description: modalContentDescription.value,
				img: snapshot.metadata.name,
				tabs: {
					tab1: {
						title: tabs[0].children[0].value,
						text: tabs[0].children[1].value
					},
					tab2: {
						title: tabs[1].children[0].value,
						text: tabs[1].children[1].value
					},
					tab3: {
						title: tabs[2].children[0].value,
						text: tabs[2].children[1].value
					}
				}
			};
				
			let specificCategorie = firestore.collection("categories").where("id", "==", modalId.value).get();
			specificCategorie
				.then(querySnapshot => {
					querySnapshot.forEach(doc => {
						doc.ref.update(changeObject);
					});
				});
		});

	});
	
	// * add a catagorie
	btnSaveAdd.addEventListener("click", function () {
		const modalContentTitle = document.querySelector("#modalAdd > .modal-content > .title");
		const modalContentDescription = document.querySelector("#modalAdd > .modal-content > #textarea");
		const tabs = document.querySelectorAll("#modalAdd .tab");
		const modalContentImg = document.querySelector("#modalAdd > .modal-content .fileInput").files[0];

		// * get file
		let storageRef = firebase.storage().ref("images/"); // eslint-disable-line no-undef, no-unused-vars
		let imgRef = storageRef.child(getId());
		imgRef.put(modalContentImg).then(snapshot => {
			const collection = firestore.collection("categories");
			collection.add({
				id: getId(),
				name: modalContentTitle.value,
				description: modalContentDescription.value,
				img: snapshot.metadata.name,
				subs: 0,
				tabs: {
					tab1: {
						title: tabs[0].children[0].value,
						text: tabs[0].children[1].value
					},
					tab2: {
						title: tabs[1].children[0].value,
						text: tabs[1].children[1].value
					},
					tab3: {
						title: tabs[2].children[0].value,
						text: tabs[2].children[1].value
					}
				}
			});
		});



	});

	// * Delete a catagorie
	btnDelete.addEventListener("click", function () {
		const modalId = document.querySelector("#modalUpdate > .modal-content > input#disabled");
		let specificCategorie = firestore.collection("categories").where("id", "==", modalId.value).get();
		specificCategorie
			.then(querySnapshot => {
				querySnapshot.forEach(doc => {
					let storageRef = firebase.storage().ref("images/");// eslint-disable-line no-undef, no-unused-vars
					let imgref = storageRef.child(doc.data().img);

					imgref.delete().then(() => {
						doc.ref.delete();
					}).catch(error => {
						console.log(error);
					});
				});
			});
	});

	// ? // FIXME Check if the right peep is online
	firebase.auth().onAuthStateChanged(function (user) { // eslint-disable-line no-undef, no-unused-vars
		if (user === null) {
			body.innerHTML = "FUCK OFF MATE IT WORKS <a href=\"./\">go back</a>";
		}
	});

	// * Functions

	function getId() {
		return "_" + Math.random().toString(36).substr(2, 9);
	}
});