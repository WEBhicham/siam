document.addEventListener("DOMContentLoaded", function(){
	const sidenav = document.querySelectorAll(".sidenav");
	const sidenavInstance = M.Sidenav.init(sidenav); // eslint-disable-line no-undef, no-unused-vars
	
	const scrollSpy = document.querySelectorAll(".scrollspy");
	const scrollSpyInstance = M.ScrollSpy.init(scrollSpy, {scrollOffset: 100}); // eslint-disable-line no-undef, no-unused-vars
	
	const modal = document.querySelectorAll(".modal");
	const modalInstance = M.Modal.init(modal); // eslint-disable-line no-undef, no-unused-vars
	
	const select = document.querySelector("select");
	const selectInstance = M.FormSelect.init(select); // eslint-disable-line no-undef, no-unused-vars
	
	const elem = document.querySelector(".datepicker");
	const instance = M.Datepicker.init(elem); // eslint-disable-line no-undef, no-unused-vars

	const collap = document.querySelectorAll(".collapsible");
	const collapInstances = M.Collapsible.init(collap, {accordion: false}); // eslint-disable-line no-undef, no-unused-vars
});

 