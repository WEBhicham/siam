document.addEventListener("DOMContentLoaded", function(){
	const firestore = firebase.firestore(); // eslint-disable-line no-undef, no-unused-vars
	const settings = { timestampsInSnapshots: true };
	const btnLoguit = document.querySelector("li > .logout");

	firestore.settings(settings);
	firebase.auth().onAuthStateChanged(function(user) { // eslint-disable-line no-undef, no-unused-vars
		if (user !== null) {
			const users = firestore.collection("users");

			users.doc(user.uid).get()
				.then(doc => {
					const data = doc.data();

					const welcometxt = document.querySelector(".welcometxt");
					const fullname = `${data.voornaam} ${data.tussenvoegsel} ${data.achternaam}`; 
					const personalInformation = `${data.geslacht}, ${data.geboortedatum}`;
					const adress = `${data.woonplaats} ${data.postcode}`;

					const welcome = `<h1>Welcome ${data.voornaam}!`;
					const [name, personal, home] = document.querySelectorAll(".collection > li");

					welcometxt.insertAdjacentHTML("beforeend", welcome);
					name.insertAdjacentHTML("beforeend", fullname);
					personal.insertAdjacentHTML("beforeend", personalInformation);
					home.insertAdjacentHTML("beforeend", adress);
				});
		}
	});

	btnLoguit.addEventListener("click", function(){
		firebase.auth().signOut().then(function() { // eslint-disable-line no-undef, no-unused-vars
			window.location = "./";
		}).catch(function(error) {
			alert(error);
		});
	});
});

 