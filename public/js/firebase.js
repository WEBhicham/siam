document.addEventListener("DOMContentLoaded", function () {
	//* firestore settings
	const firestore = firebase.firestore(); // eslint-disable-line no-undef, no-unused-vars
	const settings = { timestampsInSnapshots: true };
	firestore.settings(settings);

	//* collections  
	const users = firestore.collection("users");
	const categories = firestore.collection("categories");
	const information = firestore.collection("information");
	const werkwijze = firestore.collection("werkwijze");

	//* variables
	const inschijfBtn = document.querySelector("form > .btn");
	const loginBtn = document.querySelector(".loginBtn");

	// * **************** *//
	// * set up the pages *//
	// * **************** *//
	categories.get()
		.then(querySnapshot => {
			querySnapshot.forEach(doc => {
				if(doc.data().stonebottom === undefined){
					let data = doc.data();
					createCatogrie(data.name, data.description, data.img, data.tabs);
				}
			});
		}).catch(err => {
			console.log(err);
		});

	information.get()
		.then(querySnapshot => {
			querySnapshot.forEach(doc => {
				if(doc.data().stonebottom === undefined){
					let data = doc.data();
					createInformation(data.titel, data.preTekst, data.informatie, doc.id);
				}
			});
		});

	werkwijze.get()
		.then(querySnapshot => {
			querySnapshot.forEach(doc => {
				if(doc.data().stonebottom === undefined){
					let data = doc.data();
					createStep(data.icon, data.titel, data.tekst, data.order);
				}	
			});
		});
	// ***************************** //
	// * user management functions * //
	// ***************************** //

	loginBtn.addEventListener("click", function(){
		const [email, password] = document.querySelectorAll("#inloggen .input-field > input");
		firebase.auth().signInWithEmailAndPassword(email.value, password.value) // eslint-disable-line no-undef, no-unused-vars
			.catch(function(error) {
				console.log(`${error.code}: ${error.message}`);
			});
	});

	inschijfBtn.addEventListener("click", function(){
		const userObject = {
			voornaam: [document.getElementById("voornaam").value, true],
			tussenvoegsel: [document.getElementById("tussenvoegsel").value, false],
			achternaam: [document.getElementById("achternaam").value, true], 
			geboortedatum: [document.getElementById("datepicker").value, true],
			geslacht: [document.getElementById("geslacht").value, true],
			postcode: [document.getElementById("postcode").value, true],
			huisnummer: [document.getElementById("huisnummer").value, true],
			woonplaats: [document.getElementById("woonplaats").value, true],
			email: [document.getElementById("inschrijfEmail").value, true],
			wachtwoord: [document.getElementById("inschrijfWachtwoord").value, true], 
			wachtwoordHer: [document.getElementById("inschrijfWachtwoordHer").value, true] 
		};

		createUser(userObject);
	});

	firebase.auth().onAuthStateChanged(function(user) { // eslint-disable-line no-undef, no-unused-vars
		if (user !== null) {
			let profile = `
				<li>
					<a href="./profile.html">profile</a>
				</li>
			`;
			const li = document.querySelectorAll("nav ul > li");
			li[4].remove();		
			const navUl = document.querySelector("nav ul");
			navUl.insertAdjacentHTML("beforeend", profile);		
		}
	});

	// ************** //
	// * FUNCTIONS  * //
	// ************** //

	/**
	 * creates a user and checks the form
	 * 
	 * @param {object} userObject an user object with all user information
	 */
	function createUser(userObject) {
		let warning = "";
		for(let key in userObject){
			if(userObject[key][1] === true){
				if(userObject[key][0] === ""){
					warning += `Uw moet nog: ${key} invullen <br />`;
				}
			}
		}
		const alertFormTitle = document.querySelector("#alertForm h4");
		const alertFormContent = document.querySelector("#alertForm p");

		if(warning){
			alertFormTitle.innerHTML = "vul aub de vereiste velden in";
			alertFormContent.innerHTML = warning;
			warning = "";
		}else {
			switch(true) {
			case /\d/.test(userObject.voornaam[0]):
				warning += "Vul aub u echte naam in <br />";
			case /\d/.test(userObject.achternaam[0]):
				warning += "Vul aub u echte achternaam in <br />";
			case /\d/.test(userObject.woonplaats[0]):
				warning += "Vul aub u echte woonplaats in <br />";
			case /^[a-zA-Z]{4}\d{2}$/.test(userObject.postcode[0]):
				warning += "Vul aub een gedlige postcode in <br />";
			case userObject.wachtwoord[0] !== userObject.wachtwoordHer[0]:
				warning += "U wachtwoorden komen niet over een"; 
			default:
				warning = "";
			}
			if(warning){
				alertFormTitle.innerHTML = "U gegevens kloppen niet";
				alertFormContent.innerHTML = warning;
			}else {
				firebase.auth().createUserWithEmailAndPassword(userObject.email[0], userObject.wachtwoord[0]) // eslint-disable-line no-undef 
					.then(user => {	
						users.doc(user.uid).set({
							voornaam: userObject.voornaam[0],
							tussenvoegsel: userObject.tussenvoegsel[0],
							achternaam: userObject.achternaam[0],
							geboortedatum: userObject.geboortedatum[0],
							geslacht: userObject.geslacht[0],
							postcode: userObject.postcode[0],
							huisnummer: userObject.huisnummer[0],
							woonplaats: userObject.woonplaats[0]
						});
						alertFormTitle.innerHTML = "Uw heeft een account aan gemaakt!";
						alertFormContent.innerHTML = "Kijk in uw mail box voor een bevestings mail!";
					})	
					.catch(err => {
						alertFormTitle.innerHTML = `${err.code}`;
						alertFormContent.innerHTML = `${err.message}`;
					}); 
			}
		}
	}

	/**
	 * creates an information block in #informatie
	 * 
	 * @param {string} title title of the information block
	 * @param {string} preTekst the pre text of the information block
	 * @param {string} tekst the text of the modal
	 * @param {string} id the doc id
	 */
	function createInformation(title, preTekst, tekst, id) {
		const informatieSection = document.querySelector("#informatie .row");
		const body = document.querySelector("body");

		const information = `
			<div class="col s12 m6 l4">
				<div class="card small green darken-4 ">
					<div class="card-content">
						<span class="card-title">${title}</span>
						<p>${preTekst}</p>
						<div class="card-action">
						<a class="waves-effect btn green lighten-1 modal-trigger white-text left" href="#${id}">
							Meer informatie
						</a>
						</div>
					</div>
				</div>
			</div>
		`;

		const modal = `
			<div id="${id}" class="modal">
				<div class="modal-content">
					<h4>${title}</h4>
					<p>${tekst}</p>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
				</div>
			</div>
		`;

		informatieSection.insertAdjacentHTML("beforeend", information);
		body.insertAdjacentHTML("beforeend", modal);

		const modalinit = document.querySelectorAll(".modal");
		const modalInstance = M.Modal.init(modalinit); // eslint-disable-line no-undef, no-unused-vars
	}

	/**
	 * creates 1 step
	 * 
	 * @param {string} icon icon
	 * @param {string} title title
	 * @param {string} tekst text	
	 * @param {number} order the order
	 */
	function createStep(icon, title, tekst, order){
		const werkwijzeList = document.querySelector("#werkwijze ul.collapsible");

		const step = `
			<li style="order:${order};">
				<div class="collapsible-header green darken-3">
					<i class="material-icons">${icon}</i>
					${title}
				</div>
				<div class="collapsible-body green lighten-1">
					<span>${tekst}</span>
				</div>
			</li>
		`;

		werkwijzeList.insertAdjacentHTML("beforeend", step);
	}
	/** 
	 * make an id
	 */
	function makeid(len) {
		let text = "";
		let possible = "abcdefghijklmnopqrstuvwxyz";

		for (let i = 0; i < len; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	}
	/**
	 * this function creates an catogorie on the website
	 * 
	 * @param {string} name name of the catogrie
	 * @param {string} description description of the catogorie
	 * @param {string} img img of the catogorie
	 */
	function createCatogrie(name, description, img, tabs) {
		const sectionCategories = document.querySelector("#categorieen > .row > .row");
		let ids = [makeid(3), makeid(3), makeid(3)];
		const storageRef = firebase.storage().ref("images/"); // eslint-disable-line no-undef, no-unused-vars
		const imgRef = storageRef.child(img).getDownloadURL();

		imgRef.then((url) => {
			let categorie = `
			<div class="col s12 m4 l3">
				<div class="card large green darken-2">
					<div class="card-image">
						<img src="${url}"/>
					</div>
					<div class="card-content">
						<span class="card-title">${name}</span>
						<p>
							${description}
						</p>
					</div>
					<div class="card-tabs green lighten-1">
						<ul class="tabs tabs-fixed-width tabs-transparent">
							<li class="tab">
								<a class="active" href="#${ids[0]}">${tabs.tab1.title}</a>
							</li>
							<li class="tab">
								<a href="#${ids[1]}">${tabs.tab2.title}</a>
							</li>
							<li class="tab">
								<a href="#${ids[2]}">${tabs.tab3.title}</a>
							</li>
						</ul>
					</div>
					<div class="card-content green darken-2">
						<div id="${ids[0]}">${tabs.tab1.text}</div>
						<div id="${ids[1]}">${tabs.tab2.text}</div>
						<div id="${ids[2]}">${tabs.tab3.text}</div>
					</div>
				</div>
			</div>
		`;

			sectionCategories.insertAdjacentHTML("beforeend", categorie);
			const tabz = document.querySelectorAll("ul.tabs");
			const tabsInstance = M.Tabs.init(tabz); // eslint-disable-line no-undef, no-unused-vars
		}).catch(error => {
			console.log(error);
		});		
	}
});